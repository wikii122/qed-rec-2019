from rest_framework import serializers
from .models import Affiliation, Author, Title, Publication


class TitleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Title
        fields = ("name", "url")


class AffiliationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Affiliation
        fields = ("name", "address", "city", "country", "postcode", "url")


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = (
            "name",
            "surname",
            "title",
            "affiliations",
            "publication_set",
            "url",
            "hindex",
        )
        read_only_fields = ("hindex",)


class PublicationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Publication
        fields = ("title", "abstract", "date", "authors", "references")
