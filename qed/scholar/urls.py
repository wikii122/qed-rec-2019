from django.urls import path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from qed.scholar import views

router = routers.DefaultRouter()
router.register(r"titles", views.TitleViewSet)
router.register(r"affiliations", views.AffiliationViewSet)
router.register(r"authors", views.AuthorViewSet)
router.register(r"publications", views.PublicationViewSet)
urlpatterns = router.urls
