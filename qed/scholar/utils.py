"""
Utility functions
"""
import logging

from .models import Author
from .tasks import update_hindex

logger = logging.getLogger(__name__)


def update_all_hindices():
    logger.info("Ordering h-index refresh")
    authors = Author.objects.all()
    for author in authors:
        update_hindex.send(author.id)
