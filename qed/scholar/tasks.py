"""
Set of background jobs supporting work of application. 
"""
import dramatiq
import logging

from django.db.models import Count

from .models import Author

logger = logging.getLogger(__name__)


@dramatiq.actor
def update_hindex(author_id):
    """
    Task allowing to update Hirsch index for a given autor.
    """
    author = Author.objects.get(id=author_id)
    logger.info(f"Calculating h-index for {author.name} {author.surname}")

    publications = author.publication_set.annotate(
        reference_count=Count("publication")
    ).order_by("-reference_count")

    hindex = 0
    for idx, publication in enumerate(publications, start=1):
        if publication.reference_count > idx:
            hindex = idx

    author.hindex = hindex
    author.save()
