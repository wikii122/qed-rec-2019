"""
Run periodic tasks.
"""
import schedule
import time

from django.core.management.base import BaseCommand

from qed.scholar.utils import update_all_hindices


class Command(BaseCommand):
    help = "Runs periodic tasks"

    def handle(self, *args, **options):
        schedule.every().hour.do(update_all_hindices)

        while True:
            schedule.run_pending()
