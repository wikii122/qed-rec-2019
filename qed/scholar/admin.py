from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Affiliation, Author, Title, Publication

admin.site.register(Title)
admin.site.register(Affiliation)
admin.site.register(Author)
admin.site.register(Publication)
