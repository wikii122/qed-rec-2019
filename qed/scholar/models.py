from django.db import models

# Create your models here.
class Title(models.Model):
    """Scientific title for an author."""

    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)


class Affiliation(models.Model):
    """Organization unit, eg university."""

    name = models.CharField(max_length=256)
    address = models.TextField()
    postcode = models.CharField(max_length=10)
    city = models.CharField(max_length=128)
    country = models.CharField(max_length=5)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)


class Author(models.Model):
    """Person responsible for publication."""

    name = models.CharField(max_length=128)
    surname = models.CharField(max_length=128)
    hindex = models.IntegerField(blank=True, null=True)

    title = models.ForeignKey(Title, on_delete=models.PROTECT)
    affiliations = models.ManyToManyField(Affiliation)

    def __str__(self):
        return f"{self.name} {self.surname}"

    class Meta:
        ordering = ("name", "surname")


class Publication(models.Model):
    """Publication description."""

    title = models.CharField(max_length=256)
    abstract = models.TextField()
    date = models.DateField()

    authors = models.ManyToManyField(Author, blank=False)
    references = models.ManyToManyField("Publication", blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ("title",)
