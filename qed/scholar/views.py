from django.shortcuts import render
from rest_framework import viewsets
from .models import Affiliation, Author, Title, Publication
from .serializers import (
    AffiliationSerializer,
    AuthorSerializer,
    TitleSerializer,
    PublicationSerializer,
)
from .utils import update_all_hindices


class TitleViewSet(viewsets.ModelViewSet):
    queryset = Title.objects.all()
    serializer_class = TitleSerializer


class AffiliationViewSet(viewsets.ModelViewSet):
    queryset = Affiliation.objects.all()
    serializer_class = AffiliationSerializer


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class PublicationViewSet(viewsets.ModelViewSet):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer

    def create(self, *args, **kwargs):
        result = super(PublicationViewSet, self).create(*args, **kwargs)
        update_all_hindices()
        return result

    def update(self, *args, **kwargs):
        result = super(PublicationViewSet, self).update(*args, **kwargs)
        update_all_hindices()
        return result
