# Running

### Installing dependencies

    poetry install

### Prepare database

    poetry run python manage.py migrate

### Server

    poetry run python manage.py runserver

### Worker

    poetry run python manage.py rundramatiq

### Scheduler

    poetry run python manage.py runscheduler

## Talkthrough

The API is pretty self documenting, all endpoints are defined on `/` path.
API structure is completely flat as the data does not indicate any hierarchy.
API itself is available on `localhost:8000`.

Application requires to work RabbitMQ service running on `localhost:5672`.
All configuration variables are hardcoded, including `DEBUG` variable.
This would not be a case in real product, where all variables containg environment specific data, url and similar should be read from evironmental variables.

Due to SQLite usage worker does quite often throw an exception caused by exceeding connection pool and database lockout, I did not fix that.

All resources are identified by their URI, and relations can also be altered by simply changing urls in json responses. An ellegant enhancement would be to add short summary to those objects, presenting not just bare URI, it would also allow to limit number of queries made to server in case most commonly accessed field (ie title).

Hirsch index is updated every hour by scheduler and after each update and creation of publication in database, and is treated as author property and included in it's response.
It does refresh all authors every time, this could be optimized by looking for what could have changed (new Publication references only few authors for instance).

Program does not contain any tests, mostly because I do know yet how to mock Django models yet, and the code itself is quite coupled with those models.
But to be honest the only logic worth testing is calculating Hirsch index, everything else is framework code enhanced with hints and descriptors, thus not containg any logic. Yet few e2e test checking endpoint answers could be useful in longer timed project.